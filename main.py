import sys
try:
    import argparse
    import urllib3
    import json
except:
    print("You do not have required modules => argparse | urllib3 | json ")
    sys.exit()

class Flight:

    def __init__( self, args ):
        self.http               = urllib3.PoolManager()
        self.args               = args
        date                    = args["date"].split("-")
        self.args["date"]       = "{0}/{1}/{2}".format(date[2],date[1],date[0])
        self.args["api_url"]    = "https://api.skypicker.com/flights?partner=picky&partner_market=us&curr=EUR"

    def find(self):
        # Default options for searching flight
        api_parameters  = "{api_url}&flyFrom={from}&to={to}&dateFrom={date}&dateTo={date}".format( **self.args )

        # Add to search query parameters if there is set / option return days
        if self.args["return"]:
            api_parameters += "&typeFlight=round&daysInDestinationFrom={return}&daysInDestinationTo={return}".format( **self.args )

        # Add to search query parameters if there is set / option fastest flight
        if self.args["fastest"]:
            api_parameters += "&sort=duration"

        # Add to search query parameters if there is set oneway flight
        if self.args["one_way"]:
            api_parameters += "&typeFlight=oneway"

        # Make request to api with selected parameters
        r       = self.http.request("GET", api_parameters )

        # Response
        flight  = json.loads(r.data.decode('utf-8'))["data"]

        if len( flight ) > 0:
            return flight[0]["booking_token"]
        else:
            return False

    def booking( self, booking_token ):
        # Required data to accept post request
        data = {
                "booking_token": booking_token,
                "currency": "EUR",
                "passengers":{
                                "birthday": "1996-10-13",
                                "documentID": "1",
                                "email": "test@test.sk",
                                "title": "Mr",
                                "firstName": "Name",
                                "lastName": "lName",

                            },
                "bags": self.args["bags"]
                }

        encoded_data = json.dumps(data).encode('utf-8')
        r            = self.http.request( "POST", "http://128.199.48.38:8080/booking", body=encoded_data, headers={'Content-Type': 'application/json'} )

        # Response
        booking      = json.loads(r.data.decode('utf-8'))

        if booking["status"] == "confirmed":
            return booking["pnr"]
        else:
            return False

if __name__ == "__main__":
    # Disable / hide SSL warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    parser = argparse.ArgumentParser(description="Order your flight")
    parser.add_argument("-o","--one-way", action='store_true', help="Only one way / single ticket")
    parser.add_argument("-d","--date", required=True, help="Set date of departure")
    parser.add_argument("-r","--return", type=int, help="Set number of days for return flight")
    parser.add_argument("-c","--cheapest", action='store_true', help="Select the cheapest flight")
    parser.add_argument("-f","--fastest", action='store_true', help="Select the fastest flight")
    parser.add_argument("-b","--bags", default=0, type=int, help="Set number of baggage")
    parser.add_argument("-F","--from", required=True, help="Set airport in IATA format")
    parser.add_argument("-T","--to", required=True, help="Set airport IATA format")

    args = parser.parse_args()
    args = vars(args)
    flight = Flight(args)

    # First run... it find your flight
    booking_token = flight.find()

    if booking_token != False:
        book = flight.booking( booking_token )
        if book != False:
            print( book )
        else:
            # IF there is some prolbem with booking flight
            print( 0 )
    else:
        # IF there is no flight with selected preferences / options
        print( 0 )
